import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  styleUrls: ['home.page.scss'],
  template: `
  <section class="main-content">
    <widget-button label='Widget 1' icon='check' />
    <widget-button label='Widget 2' icon='account_circle' />
    <widget-button label='Widget 3' icon='folder' />
    <widget-button label='Widget 4' icon='error' />
  </section>
  `
})

export class HomePage implements OnInit {
  constructor() { }

  ngOnInit() { }
}
