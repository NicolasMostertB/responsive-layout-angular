import { NgModule } from '@angular/core';

import { HomePage } from './pages/home.page';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../shared/shared.mocule';

@NgModule({
  imports: [
    MatIconModule,
    SharedModule,
  ],
  exports: [HomePage],
  declarations: [HomePage],
  providers: [],
})
export class HomeModule { }
