import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './features/home/pages/home.page';
import { NgModule } from '@angular/core';

export const routes: Routes = [

  {
    path: 'home',
    component: HomePage
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
