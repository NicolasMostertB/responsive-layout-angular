import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit, inject } from '@angular/core';
import { Subject, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'app-nav-bar',
  styleUrls: ['nav-bar.component.scss'],
  templateUrl: 'nav-bar.component.html'
})

export class NavBarComponent implements OnInit {
  private breakpointObserver = inject(BreakpointObserver);

  private destroy$ = new Subject<void>();

  public isMobile: boolean;

  ngOnInit() {
    this.breakpointObserver
      .observe(['(max-width: 768px'])
      .pipe(
        takeUntil(this.destroy$),
        tap((state: BreakpointState) => this.isMobile = state.matches)
      ).subscribe();
  }
}
