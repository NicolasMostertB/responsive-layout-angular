import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
  <style>
    footer {
      display: flex;
      padding: 1.5rem;
      justify-content: center;
      background-color: rgb(221, 221, 221);
    }
  </style>

  <footer>
    <h2>Footer</h2>
  </footer>`
})

export class FooterComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
