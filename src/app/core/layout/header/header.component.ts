import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
  <style>
    header {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      background-color: rgb(26, 188, 156);
      color: white;
      padding-top: 3rem;
      padding-bottom: 3rem;

      h1 {
        margin-bottom: 0, 5rem;
      }
    }

    #info {
      background:yellow;
      padding:5px;

      h4 {
        text-align:center
      }
    }

    @media only screen and (max-width: 768px) {
      #info {
        display: none;
      }
    }
  </style>



  <section id="info">
    <h4>Resize the browser window to see the responsive effect.</h4>
  </section>

  <header>
    <h1>My Website</h1>
    <p>With a <strong>flexible</strong> layout</p>
  </header>
  `
})

export class HeaderComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
