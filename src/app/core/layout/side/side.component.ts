import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side',
  template: `
  <style>
    .side-content {
      position: sticky;
      padding: .75rem 2rem;
      background-color: rgb(241, 241, 241);
      display: flex;
      flex-direction: column;

      img {
        align-self: center;
        height: 200px;
        width: auto;
      }
    }
  </style>

  <aside class="side-content">
    <h2>About Me</h2>
    <h5>Photo of me:</h5>
    <img src="https://scontent.fymq3-1.fna.fbcdn.net/v/t1.18169-9/12801474_10205151017928061_4524143159191012725_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=be3454&_nc_ohc=P_vIfTCJyh8AX8CXKel&_nc_ht=scontent.fymq3-1.fna&oh=00_AfD6zv-vsl-usFSeYdZ9iRjz_PsoCqOl9Mx1HD68AsyABg&oe=6601ECDD" />
    <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
    <h3>More Text</h3>
    <p>Lorem ipsum dolor sit ame.</p>
    <div class="fakeimg" style="height:60px;">Image</div><br>
    <div class="fakeimg" style="height:60px;">Image</div><br>
    <div class="fakeimg" style="height:60px;">Image</div>
  </aside>
  `
})

export class SideComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}
