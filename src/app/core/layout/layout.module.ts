import { NgModule } from '@angular/core';
import { FooterComponent } from './footer/footer.component';
import { MatIconModule } from '@angular/material/icon';
import { HeaderComponent } from './header/header.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SideComponent } from './side/side.component';
import { LayoutModule as CdkLayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    CdkLayoutModule,
    MatRippleModule
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    NavBarComponent,
    SideComponent
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    NavBarComponent,
    SideComponent
  ],
  providers: [],
})
export class LayoutModule { }
