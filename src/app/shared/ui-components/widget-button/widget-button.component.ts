import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'widget-button',
  styleUrls: ['widget-button.component.scss'],
  template: `
  <button class="main-content-item">
    <mat-icon>{{ icon }}</mat-icon>
    <h3>{{ label }}</h3>
  </button>
  `
})

export class WidgetButtonComponent implements OnInit {
  @Input() label: string;
  @Input() icon: string;

  ngOnInit() { }
}
