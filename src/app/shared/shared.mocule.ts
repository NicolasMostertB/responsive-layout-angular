import { NgModule } from '@angular/core';

import { WidgetButtonComponent } from './ui-components/widget-button/widget-button.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [MatIconModule],
  exports: [WidgetButtonComponent],
  declarations: [WidgetButtonComponent],
  providers: [],
})
export class SharedModule { }
